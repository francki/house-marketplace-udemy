// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import {getFirestore} from 'firebase/firestore';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDxUUGxVUFfjtGzrO-eGwkOhmc6a60DEZ8",
  authDomain: "house-marketplace-app-a9341.firebaseapp.com",
  projectId: "house-marketplace-app-a9341",
  storageBucket: "house-marketplace-app-a9341.appspot.com",
  messagingSenderId: "345715400490",
  appId: "1:345715400490:web:0e0fb0ea74461a6263c853"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getFirestore()